from peewee import SqliteDatabase, Model, CharField, ForeignKeyField

db = SqliteDatabase('arguments.db')


class Argument(Model):
    argument = CharField()
    regex = CharField(null=True)

    class Meta:
        database = db


class Counterargument(Model):
    counterargument = CharField()
    argument = ForeignKeyField(Argument)
    source = CharField(null=True)

    class Meta:
        database = db


if __name__ == '__main__':
    db.create_tables([Argument, Counterargument])
